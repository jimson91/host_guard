# Host Guard

### Description
Host Guard is an interactive shell script that acts as a simple Host-based Intrusion Detection System (HIDS) for Linux systems. The script takes in a single argument of a target directory and stores and encrypts a recursive listing of file system attributes for future comparison. The goal is to provide file integrity checks that detect the following modifications of a file:
 
* Modification date
* Permission
* Owner
* Group 
* SHA256 Hash

The script is also dependant on command-line software tools that are available for most Linux distributions.

### Tools required

* <a href="https://www.man7.org/linux/man-pages/man1/ls.1.html">ls</a> – Lists the contents/attributes of a directory and provides many
options to filter listings.

* <a href="https://man7.org/linux/man-pages/man1/diff.1.html">diff</a> – Compares the standard input of two files line by line and
outputs the differences while providing many output filtering options.

* <a href="https://man7.org/linux/man-pages/man1/find.1.html">find</a> – Searches for files and directories in the Linux file system
and additionally provides the ability to execute other programs

* <a href="https://man7.org/linux/man-pages/man1/sha256sum.1.html">sha256sum</a> – Computes and checks the SHA256 message digest
of specific files

* <a href="https://linux.die.net/man/1/openssl">openssl</a> – Supplies various cryptography functions of OpenSSL's cryptographic library from the shell.