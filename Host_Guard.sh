#!/bin/bash

# Define Directories, Log Files and README
OPTION=$1
NAME=$2
TARGET_DIR=$3
ARG_COUNT=$#
SNAP_DIR='/home/'$USER'/.snapshot'
INFO_FILE="$SNAP_DIR/$NAME-info.txt"
HASH_FILE="$SNAP_DIR/$NAME-sums.txt"
SNAP_FOUND=0

# Color codes
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# Check if target directory exists, if not exit
function read_target() {
    initialise
    if [ -d $TARGET_DIR ]; then
        read_options
    else
        echo "Target directory does not exist"
    fi
}

function display_usage() {
    echo "usage: [-option] [snapshot name] [target directory]"
    echo "options:"
    echo "  -c      Create snapshot file"
    echo "  -i      Compare File/Directory Attributes"
    echo "  -h      Perform Checksum Test"
    echo "  -o      Ouput snapshot files to terminal"
    echo "  -r      Remove snapshot file"
    exit 1
}

function decrypt_snapshot() {
    read -s -p "Enter snapshot password:`echo $'\n> '` " password
    if [ "$#" == "1" ]; then
        DECRYPTED_OUTPUT=$(openssl enc -aes-256-cbc -d -pbkdf2 -iter 20000 -in $1 -pass pass:$password 2>&1)
        elif [ "$#" == "2" ]; then
        DECRYPTED_OUTPUT=$(openssl enc -aes-256-cbc -d -pbkdf2 -iter 20000 -in $1 -pass pass:$password 2>&1)
        DECRYPTED_OUTPUT_2=$(openssl enc -aes-256-cbc -d -pbkdf2 -iter 20000 -in $2 -pass pass:$password 2>&1)
    fi
    if [ $? -ne 0 ]; then
        echo "Failed to decrypt snapshot. Incorrect Password"
    fi
}

function output_snapshot() {
    if [ "$SNAP_FOUND" -eq 1 ]; then
        decrypt_snapshot $HASH_FILE $INFO_FILE
        printf "\n\n#### Checksum Listing ####\n\n"
        echo "$DECRYPTED_OUTPUT"
        printf "\n\n#### File Listing ####\n\n"
        echo "$DECRYPTED_OUTPUT_2"
    else
        printf "No snapshot files detected for name '$NAME'\nUse '-c [snapshot name] [target directory]'\n"
    fi
}

# Define a function that checks the number of arguments and passes them to functions
function read_options() {
    if [ "$ARG_COUNT" == "3" ]; then
        case $OPTION in
            -c)
            log_check;;
            -o)
            output_snapshot;;
            -h)
            check_integrity;;
            -i)
            check_info;;
            *)
            display_usage;;
        esac
        elif [ "$ARG_COUNT" == "1" ]; then
        case $OPTION in
            -r)
            remove_snapshot;;
            *)
            display_usage;;
        esac
    else
        display_usage
    fi
}

function dependancy_check() {
    if  ! command -v sha256sum >/dev/null 2>&1; then
        echo "Required program not found! Missing sha256sum."
        exit 1
        elif ! command -v openssl >/dev/null 2>&1; then
        echo "Required program not found! Missing openssl."
        exit 1
        elif ! command -v find >/dev/null 2>&1; then
        echo "Required program not found! Missing find."
        exit 1
        elif ! command -v ls >/dev/null 2>&1; then
        echo "Required program not found! Missing ls."
        exit 1
        elif ! command -v diff >/dev/null 2>&1; then
        echo "Required program not found! Missing diff."
        exit 1
    fi
}

#Check if the ".snapshot" directory/files exist
function initialise() {
    dependancy_check
    # First: Check existance of directory
    if [ ! -d $SNAP_DIR ]; then
        SNAP_FOUND=0
    else
        SNAP_FOUND=1
    fi
    # Second: Check existance of files
    if [ -f $HASH_FILE ] && [ -f $INFO_FILE ]; then
        SNAP_FOUND=1
    else
        SNAP_FOUND=0
    fi
}

function log_check() {
    # Check if the .snapshot" directory already exists, if true ask permission to overwrite
    if [ -d $SNAP_DIR ]; then
        if [ -f $INFO_FILE ] && [ -f $HASH_FILE ]; then
            echo "Snapshot already exists for target directory"
            while true; do
                printf "Would you like to overwrite the previous files? [y/n]\n"
                read -r -p "> " CONFIRM
                case $CONFIRM in
                    [Yy]* )
                        create_snapshot
                    break ;;
                    [Nn]* )
                    break ;;
                    * )
                    echo "Please answer yes or no." ;;
                esac
            done
        else
            create_snapshot
        fi
    else
        mkdir $SNAP_DIR
        create_snapshot
    fi
}

function create_snapshot() {
    read -s -p "Enter password to secure snapshot files:`echo $'\n> '` " password
    if [ ! ${#password} -lt 6 ]; then
        echo "Generating Snapshot Files. Please wait..."
        local FILE_LIST=$(ls -lR $TARGET_DIR)
        local HASH_LIST=$(find $TARGET_DIR -type f -exec sha256sum '{}' \;)
        echo "$FILE_LIST" | openssl enc -aes-256-cbc -salt -pbkdf2 -iter 20000 -pass pass:$password > $INFO_FILE
        echo "$HASH_LIST" | openssl enc -aes-256-cbc -salt -pbkdf2 -iter 20000 -pass pass:$password > $HASH_FILE
        if [ $? -eq 0 ]; then
            echo "Snapshot Files Saved in $SNAP_DIR"
        else
            echo "Unknown error occured. Status code: $?"
            exit 1
        fi
    else
        echo "Password must be at least 6 characters long"
    fi
}

function remove_snapshot() {
    if [ "$SNAP_FOUND" -eq 1 ]; then
        rm -r $SNAP_DIR
        printf "Snapshot directory cleared\n"
    else
        printf "Snapshot directory cleared\n"
    fi
}

function check_integrity() {
    if [ "$SNAP_FOUND" -eq 1 ]; then
        decrypt_snapshot $HASH_FILE
        echo -e "\nIn progress..."
        local COMPARE_OUT=$(echo "$DECRYPTED_OUTPUT" | sha256sum --quiet --ignore-missing -c 2>&1)
        if [ -z "$COMPARE_OUT" ]; then
            echo "Checksum Test PASSED"
            echo "${green}No files were modified${reset}"
        else
            echo "Checksum Test FAILED: The following files have been modified"
            printf "\n${red}$COMPARE_OUT${reset}\n\n"
        fi
    else
        printf "No snapshot files detected for name '$NAME'\nUse '-c [snapshot name] [target directory]'\n"
    fi
}

function check_info() {
    if [ "$SNAP_FOUND" -eq 1 ]; then
        decrypt_snapshot $INFO_FILE
        echo -e "\nIn progress..."
        local TARGET_LIST=$(ls -lR $TARGET_DIR)
        local ORIGINAL="$(diff -r --changed-group-format='%<' --unchanged-group-format='' <(echo "$DECRYPTED_OUTPUT") <(echo "$TARGET_LIST"))"
        local MODIFIED="$(diff -r --changed-group-format='%>' --unchanged-group-format='' <(echo "$DECRYPTED_OUTPUT") <(echo "$TARGET_LIST"))"
        if [ -z "$MODIFIED" ]; then
            echo "File Check PASSED"
            echo "${green}No files were modified${reset}"
        else
            printf "File Check FAILED: The following files have been modified:\n\n"
            printf "===== ORIGINAL =====\n${green}$ORIGINAL${reset}\n\n"
            printf "===== MODIFIED =====\n${red}$MODIFIED${reset}\n\n"
        fi
    else
        printf "No snapshot files detected for name '$NAME'\nUse '-c [snapshot name] [target directory]'\n"
    fi
}

read_target